libcrypt-dsa-perl (1.19-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.19.
    - This release resolves CVE-2011-3599
    (Fixed in Debian with 1.17-3, cf. #644189)
  * Drop debian/patches/remove-fallback, applied upstream.
    (CVE-2011-3599, #644189)
  * Drop debian/patches/00_use_digest_sha.diff, applied upstream.
  * Update debian/upstream/metadata.
  * Module::Install is gone.
  * Update years of upstream copyright.
  * New upstream maintainer.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Add test and runtime dependency on libcrypt-urandom-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Dec 2024 00:27:16 +0100

libcrypt-dsa-perl (1.17-5) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 22:21:04 +0100

libcrypt-dsa-perl (1.17-4) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Dominic Hargreaves ]
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 01 Sep 2015 00:26:38 +0100

libcrypt-dsa-perl (1.17-3) unstable; urgency=low

  * Team upload.
  * Add patch to remove fallback to Data::Random (Closes: #644189).

 -- Harlan Lieberman-Berg <H.LiebermanBerg@gmail.com>  Mon, 03 Oct 2011 14:39:15 -0400

libcrypt-dsa-perl (1.17-2) unstable; urgency=low

  * Team upload.
  * Drop libdata-random-perl from Depends and Build-Depends-Indep, all
    kernels in Debian should have /dev/random nowadays. (Closes: #644036)

 -- gregor herrmann <gregoa@debian.org>  Mon, 03 Oct 2011 17:27:21 +0200

libcrypt-dsa-perl (1.17-1) unstable; urgency=low

  * Initial Release. (Closes: #441989)

 -- Julián Moreno Patiño <darkjunix@gmail.com>  Wed, 28 Sep 2011 00:22:01 -0500
